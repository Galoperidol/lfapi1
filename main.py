import platform

import uvicorn
from fastapi import FastAPI
from pydantic import BaseModel
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


class Item(BaseModel):
    name: str
    age: int


@app.get("/")
def get_main():
    return {'some': 'data1'}


@app.get("/items/{item_id}")
def get_something(item_id: int, q=None, b=None):
    return {"item_id": item_id, "q": q, "b": b}


@app.post("/items/")
def post_something(item: Item):
    return item


if platform.system() == 'Windows':
    if __name__ == '__main__':
        uvicorn.run("main:app", port=8000, reload=True, access_log=False)